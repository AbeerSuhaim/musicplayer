package com.example.pc.music;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;


public class songsAdapter extends ArrayAdapter<songs> {

    private Context context;

    public songsAdapter(Activity context, ArrayList<songs> song) {
        super(context, 0, song);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;

        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }


        listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context , playActivity.class);
                context.startActivity(i);
            }
        });

        songs currentSongArtist = getItem(position);

        TextView nameTextView = (TextView) listItemView.findViewById(R.id.Artist_name);
        nameTextView.setText(currentSongArtist.getArtistName());


        TextView songTextView = (TextView) listItemView.findViewById(R.id.Song_name);
        songTextView.setText(currentSongArtist.getSongName());

        ImageView iconView = (ImageView) listItemView.findViewById(R.id.Play_icon);
        iconView.setImageResource(currentSongArtist.getPlayIcon());
        return listItemView;

    }


}
