package com.example.pc.music;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;


public class albumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        Button homeButton= (Button) findViewById(R.id.home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(albumActivity.this,MainActivity.class));
            }
        });

        Button playButton= (Button) findViewById(R.id.play_list);
        playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(albumActivity.this,playActivity.class));
            }
        });

       ArrayList<Integer > albums = new ArrayList<>();
       albums.add(R.drawable.talal);
       albums.add (R.drawable.alromi);
       albums.add(R.drawable.abadi);

        LinearLayout albumsList = ( LinearLayout) findViewById(R.id.album_list);

        int index=0;
        while( index < albums.size()){
            ImageView albumView = new  ImageView(this);
            albumView.setImageResource(albums.get(index));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( 1080, 600);
            albumView.setLayoutParams(layoutParams);
            albumsList.addView(albumView);
            index++;
        }
    }
}
