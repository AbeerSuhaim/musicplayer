package com.example.pc.music;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;


import java.util.ArrayList;


public class playActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        Button homeButton= (Button) findViewById(R.id.home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(playActivity.this,MainActivity.class));
            }
        });
        Button albumsButton= (Button) findViewById(R.id.album1);
        albumsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(playActivity.this,albumActivity.class));
            }
        });

        ArrayList<songs> songArtist  = new ArrayList<songs>();

        songArtist.add(new songs("Abadi", "Tadreen", R.drawable.play_icon));
        songArtist.add(new songs("Abadi", "Amani", R.drawable.play_icon));
        songArtist.add(new songs("Abadi", "Tabeeh", R.drawable.play_icon));
        songArtist.add(new songs("M Alroumi", "Kalimat", R.drawable.play_icon));
        songArtist.add(new songs("M Alroumi", "Kon Sadiki", R.drawable.play_icon));
        songArtist.add(new songs("Talal", " Zaman al Samt", R.drawable.play_icon));
        songArtist.add(new songs("Talal", "Magadir ", R.drawable.play_icon));
        songArtist.add(new songs("Talal", "Dhalem ", R.drawable.play_icon));
        songArtist.add(new songs("Talal", "Ya Teflaten ", R.drawable.play_icon));

        songsAdapter adapter = new songsAdapter(this, songArtist);
        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }
}
