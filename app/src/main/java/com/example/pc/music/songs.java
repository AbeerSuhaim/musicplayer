package com.example.pc.music;

public class songs {

    private String artistName;
    private String songName;
    private int playIcon;

    public songs(String aName, String sName, int playImage)

    {
        artistName = aName;
        songName = sName;
        playIcon = playImage;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getSongName() {
        return songName;
    }

    public int getPlayIcon() {
        return playIcon;
    }

}
